# Vue.js Basics

## 04: What is Vue.js

- runs purely in browser
- handles many actions, preventing multiple requests
- very small file size
- lots of features
- shallow learning curve
- it's fucking awesome

---

## 05: Setting up Vue.js (the simple way)

[VueJS Guide](https://vuejs.org/v2/guide/)

### index.html

```html
<!DOCTYPE html>
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Vue Basics</title>
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="">
  </head>
  <body>  
  <h1>Vue Basics</h1>
  <div id="app"></div>
  <script src="app.js" async defer></script>
  </body>
</html>
```

---

## 06: The Vue Instance

### index.html

```html
<!DOCTYPE html>
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Vue Basics</title>
  <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="">
  </head>
  <body>  
  <h1>Vue Basics</h1>
  <div id="app">
    <h2>{{ title }}</h2> <!-- added this -->
  </div>
  <script src="app.js" async defer></script>
  </body>
</html>
```

### app.js

```js
new Vue({
  el: '#app',
  data: {
  title: 'Becoming a Vue Ninja'
  }
})
```

---

## 07: Methods

### index.html

```html
<body>  
  <h1>Vue Basics</h1>
  <div id="app">
    <h2>{{ title }}</h2>
    <p>{{ greet("evening") }}</p>
  </div>
  <script src="app.js" async defer></script>
</body>
```

### app.js

```js
new Vue({
  el: '#app',
  data: {
    title: 'Becoming a Vue Ninja',
    name: 'Ryu'
  },
  methods: {
    greet(time) {
      return `Hello and good ${time}, ${this.name}`
    }
  }
})
```

---

## 08: Data Binding

### index.html

```html
<!DOCTYPE html>
  <head>
    <title>Vue Basics</title>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <style>
      .one {
        color: green;
      }
      .two {
        text-decoration: line-through;
      }
    </style>
  </head>
  <body>  
    <h1>Vue Basics</h1>
    <div id="app">
      <h2>{{ title }}</h2>
      <p>{{ greet("evening") }}</p>
      <a :href="url">YouTube</a>
      <div :class="classes">Classes from Instance</div>
      <input type="text" :value="name">
    </div>
    <script src="app.js" async defer></script>
  </body>
</html>
```

### app.js

```js
new Vue({
  el: '#app',
  data: {
    title: 'Becoming a Vue Ninja',
    name: 'Ryu',
    url: 'http://www.youtube.com',
    classes: ['one', 'two']
  },
  methods: {
    greet(time) {
      return `Hello and good ${time}, ${this.name}`
    }
  }
})
```

### so far we have this

![image](./images/Lesson008.png)

---

## 09: Events

### index.html

```html
<!DOCTYPE html>
  <head>
    <title>Vue Basics</title>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  </head>
  <body>  
    <h1>Vue Basics</h1>
    <div id="app">
      <h2>{{ title }}</h2>
      <p>I earn {{ wage }} pounds per hour</p>
      <button @click="changeWage(1)">Increase wage by £1</button>
      <button @click="changeWage(-1)">Decrease wage by £1</button>
      <button @dblclick="changeWage(5)">Increase wage by £5</button>
      <button @dblclick="changeWage(-5)">Decrease wage by £5</button>
    </div>
    <script src="app.js" async defer></script>
  </body>
</html>
```

### app.js

```js
new Vue({
  el: '#app',
  data: {
    title: 'Becoming a Vue Ninja',
    wage: 10
  },
  methods: {
    changeWage(amount) {
      this.wage += amount
    }
  }
})
```

### results

![image](./images/Lesson009.png)

---

## 10: The Event Object

### index.html

```html
<!DOCTYPE html>
  <head>
    <title>Vue Basics</title>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <style>
      .canvas {
        width: 300px;
        height: 300px;
        margin-top: 20px;
        background: #ddd
      }
    </style>
  </head>
  <body>  
    <h1>Vue Basics</h1>
    <div id="app">
      <h2>{{ title }}</h2>
      <button @click="logEvent">Log Event Info</button>
      <div class="canvas" @mousemove="logCoords">{{ coords.x }}, {{ coords.y }}</div>
    </div>
    <script src="app.js" async defer></script>
  </body>
</html>
```

### app.js

```js
new Vue({
  el: '#app',
  data: {
    title: 'Becoming a Vue Ninja',
    coords: {
      x: 0,
      y: 0
    }
  },
  methods: {
    logEvent(e) {
      console.log(e)
    },
    logCoords(e) {
      this.coords.x = e.offsetX
      this.coords.y = e.offsetY
    }
  }
})
```

### results

![image](./images/Lesson010.png)

---

## 11: Keyboard Events

### index.html

```html
<!DOCTYPE html>
  <head>
    <title>Vue Basics</title>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  </head>
  <body>  
    <h1>Vue Basics</h1>
    <div id="app">
      <h2>{{ title }}</h2>
      <p>My Name is {{ name }}</p>
      <input type="text" @keyup="updateName">
      <input type="text" @keypress="updateName">
    </div>
    <script src="app.js" async defer></script>
  </body>
</html>
```

### app.js

```js
new Vue({
  el: '#app',
  data: {
    title: 'Becoming a Vue Ninja',
    name: 'Ryu'
  },
  methods: {
    updateName(e) {
      // console.log(e.target.value)
      this.name = e.target.value
    }
  }
})
```

### results

- left box uses `keyup`, right uses `keypress`
- since right updates when the key is pressed, it is a letter behind

![image](./images/Lesson011.png)

---

## 12: Two-way Data Binding (`v-model`)

### index.html

```html
<!DOCTYPE html>
  <head>
    <title>Vue Basics</title>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  </head>
  <body>  
    <h1>Vue Basics</h1>
    <div id="app">
      <h2>{{ title }}</h2>
      <p>My Name is {{ name }}</p>
      <input type="text" v-model="name">
    </div>
    <script src="app.js" async defer></script>
  </body>
</html>
```

### app.js

```js
new Vue({
  el: '#app',
  data: {
    title: 'Becoming a Vue Ninja',
    name: 'Ryu'
  },
  methods: {

  }
})
```

---

## 13: Modifiers

### index.html

```html
<!DOCTYPE html>
  <head>
    <title>Vue Basics</title>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  </head>
  <body>  
    <h1>Vue Basics</h1>
    <div id="app">
      <h2>{{ title }}</h2>
      <p>My Name is {{ name }}</p>
      <button @click.alt="logMessage">alt-click me</button> <!-- only works with alt-click -->
      <button @click.shift="logMessage">shift-click me</button> <!-- only works with shift + click -->
      <a href="http://www.google.com" @click.prevent="logMessage">Google</a> <!-- prevents typical response, doesn't go to website -->
    </div>
    <script src="app.js" async defer></script>
  </body>
</html>
```

### app.js

```js
new Vue({
  el: '#app',
  data: {
    title: 'Becoming a Vue Ninja',
    name: 'Ryu'
  },
  methods: {
    logMessage() {
      console.log('Hello World');
    }
  }
})
```

---

## 14: Conditional Output with `v-if`

### index.html

```html
<!DOCTYPE html>
  <head>
    <title>Vue Basics</title>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  </head>
  <body>  
    <h1>Vue Basics</h1>
    <div id="app">
      <h2>{{ title }}</h2>
      <p v-if="showName">Mario</p> <!--if true, age never shows-->
      <p v-else-if="showAge">25</p> <!-- if true and name is false, this shows-->

      <button @click="toggleName">ShowName: {{ showName }}</button>
      <button @click="toggleAge">ShowAge: {{ showAge }}</button>
    </div>
    <script src="app.js" async defer></script>
  </body>
</html>
```

### app.js

```js
new Vue({
  el: '#app',
  data: {
    title: 'Becoming a Vue Ninja',
    showName: false,
    showAge: true
  },
  methods: {
    toggleName() {
      this.showName = !this.showName
    },
    toggleAge() {
      this.showAge = !this.showAge
    }
  }
})
```

---

## 15: Looping with `v-for`

### index.html

```html
<!DOCTYPE html>
  <head>
    <title>Vue Basics</title>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
  </head>
  <body>  
    <h1>Vue Basics</h1>
    <div id="app">
      <h2>{{ title }}</h2>
      <ul>
        <li v-for="item in items">{{ item }}</li>
      </ul>
      <div v-for="(ninja, index) in ninjas">
        <p>{{ index }} - {{ ninja.name }}</p>
        <p>Age: {{ ninja.age }}</p>
        <p>Belt Color: {{ ninja.belt }}</p>
      </div>
    </div>
    <script src="app.js" async defer></script>
  </body>
</html>
```

### app.js

```js
new Vue({
  el: '#app',
  data: {
    title: 'Becoming a Vue Ninja',
    items: ['Mushroom', 'Green Shells', 'Red Shells', 'Banana', 'Star'],
    ninjas: [
      { name: 'Crystal', age: 25, belt: 'Black'},
      { name: 'Ryu', age: 30, belt: 'Brown'},
      { name: 'Ken', age: 35, belt: 'Orange'}
    ]
  },
  methods: {

  }
})
```

### results

![image](./images/Lesson015.png)

---
