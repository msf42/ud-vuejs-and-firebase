# Project Three - Geo Ninjas

## 72: Project Overview & Setup

```bash
vue create geo-ninjas
```

- added router and babel

---

## 73: Project Structure

![image](images/Lesson073.png)

---

## 74: Setting up Firebase

- set up firebase
- added firebase/init.js

### init.js

```js
import firebase from 'firebase'
import firestore from 'firebase/firestore'

// Initialize Firebase
var firebaseConfig = {
  apiKey: "AIzaSyBHXuTzkj9_Jy_71swYdp1q0-_m9tFS9Po",
  authDomain: "geo-ninjas-3387b.firebaseapp.com",
  databaseURL: "https://geo-ninjas-3387b.firebaseio.com",
  projectId: "geo-ninjas-3387b",
  storageBucket: "geo-ninjas-3387b.appspot.com",
  messagingSenderId: "839561956603",
  appId: "1:839561956603:web:db30a083f910a01072b45e"
};
// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);

export default firebaseApp.firestore()
```

---

## 75: Navbar Component

- added materialize

### App.vue

```js
<template>
  <div id="app">
    <Navbar />
    <router-view/>
  </div>
</template>

<script>
import Navbar from '@/components/layout/Navbar'
export default {
  name: 'App',
  components: {
    Navbar
  }
}
</script>
```

### Navbar.vue

```vue
<template>
  <div>
    <nav class="deep-purple darken-1">
      <div class="container">
        <a href="" class="brand-logo left">GeoNinjas!</a>
        <ul class="right">
          <li><a href="">Signup</a></li>
          <li><a href="">Login</a></li>
        </ul>
      </div>
    </nav>
  </div>
</template>

<script>
export default {
  name: 'Navbar',
  data() {
    return {

    }
  }
}
</script>

<style>
  
</style>
```

### browser view

![image](images/Lesson075.png)

---

## 76: Map Component

- updated router/index.js

```js
import Vue from 'vue'
import VueRouter from 'vue-router'
import GMap from '@/components/home/GMap'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'GMap',
    component: GMap
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
```

- made home/GMap.vue

```vue
<template>
  <div class="map">
    <h2>
      Map
    </h2>
  </div>
</template>

<script>
export default {
  name: 'GMap',
  data () {
    return {

    }
  }
}
</script>
```

---

## 77: Google Maps API

- signed up to use Google Maps JavaScript API
- added script to index.html

```html
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCtMS2mP7Sppbf91pYd2sTkCAbkTgLYjJE"></script>

```

---

## 78: Creating a New Map

### updated GMap.vue

```vue
<template>
  <div class="map">
    <div class="google-map" id="map">

    </div>
  </div>
</template>

<script>
export default {
  name: 'GMap',
  data () {
    return {
      lat: 53,
      lng: -2
    }
  },
  methods: {
    renderMap() {
      const map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: this.lat, lng: this.lng},
        zoom: 6,
        maxZoom: 15,
        minZoom: 3,
        streetViewControl: false
      })
    }
  },
  mounted() {
    this.renderMap()
  }
}
</script>

<style>
.google-map {
  width: 100%;
  height: 100%;
  margin: 0;
  background: #fff;
  position: absolute;
  top: 0;
  left: 0;
  z-index: -1;
}
</style>
```

---

## 79: Creating a Signup Page

### Signup.vue

```vue
<template>
  <div class="signup container">
    <form @submit.prevent="signup" class="card-panel">
      <h2 class="center deep-purple-text">Signup</h2>
      <div class="field">
        <label for="email">Email:</label>
        <input type="email" name="email" v-model="email">
      </div>
      <div class="field">
        <label for="password">Password:</label>
        <input type="password" name="password" v-model="password">
      </div>
      <div class="field">
        <label for="alias">Alias:</label>
        <input type="text" name="alias" v-model="alias">
      </div>
      <div class="field center">
        <button class="btn deep-purple">Signup</button>
      </div>
    </form>
  </div>
</template>

<script>
export default {
  name: 'Signup',
  data () {
    return {
      email: null,
      password: null,
      alias: null
    }
  },
  methods: {
    signup () {

    }
  }
}
</script>

<style>
.signup {
  max-width: 400px;
  margin-top: 60px;
}
.signup h2 {
  font-size: 2.4em;
}
.signup .field {
  margin-bottom: 16px;
}
</style>
```

---

## 80: Firebase Auth & Data Structure

![image](images/Lesson080.png)

- turned on email authentication in Firebase
- will set up in next lecture

---

## 81: Checking if an Alias Exists

### Signup.vue

```vue
<template>
  <div class="signup container">
    <form @submit.prevent="signup" class="card-panel">
      <h2 class="center deep-purple-text">Signup</h2>
      <div class="field">
        <label for="email">Email:</label>
        <input type="email" name="email" v-model="email">
      </div>
      <div class="field">
        <label for="password">Password:</label>
        <input type="password" name="password" v-model="password">
      </div>
      <div class="field">
        <label for="alias">Alias:</label>
        <input type="text" name="alias" v-model="alias">
      </div>
      <p class="red-text center" v-if="feedback">{{ feedback }}</p>
      <div class="field center">
        <button class="btn deep-purple">Signup</button>
      </div>
    </form>
  </div>
</template>

<script>
import slugify from 'slugify'
import db from '@/firebase/init'

export default {
  name: 'Signup',
  data () {
    return {
      email: null,
      password: null,
      alias: null,
      feedback: null,
      slug: null
    }
  },
  methods: {
    signup () {
      if (this.alias) {
        this.slug = slugify(this.alias, {
          replacement: "-",
          remove: /[$*_+~.()'"!\-:@]/g,
          lower: true
        })
        let ref = db.collection('users').doc(this.slug)
        ref.get().then(doc => {
          if (doc.exists) {
            this.feedback = 'This alias already exists'
          } else {
            this.feedback = 'This alias is free to use'
          }
        })
        console.log(this.slug)
      } else {
        this.feedback = "You must enter an alias"
      }
    }
  }
}
</script>

<style>
.signup {
  max-width: 400px;
  margin-top: 60px;
}
.signup h2 {
  font-size: 2.4em;
}
.signup .field {
  margin-bottom: 16px;
}
</style>
```

---

## 82: Signing up a User

### updated the `signup()` method

```js
signup () {
  if (this.alias && this.email && this.password) {
    this.slug = slugify(this.alias, {
      replacement: "-",
      remove: /[$*_+~.()'"!\-:@]/g,
      lower: true
    })
    let ref = db.collection('users').doc(this.slug)
    ref.get().then(doc => {
      if (doc.exists) {
        this.feedback = 'This alias already exists'
      } else {
        firebase.auth().createUserWithEmailAndPassword(this.email, this.password)
        .catch(err => {
          console.log(err)
          this.feedback = err.message
        })
        this.feedback = 'This alias is free to use'
      }
    })
    console.log(this.slug)
  } else {
    this.feedback = "You must enter all fields"
  }
}
```

---

## 83: Creating Firestone User Records

- well that was a giant pain, but all is working now thanks to the updated code

### Signup.vue (signup method)

```js
signup () {
  if (this.alias && this.email && this.password) {
    this.slug = slugify(this.alias, {
      replacement: "-",
      remove: /[$*_+~.()'"!\-:@]/g,
      lower: true
    })
    let ref = db.collection('users').doc(this.slug)
    ref.get().then(doc => {
      if (doc.exists) {
        this.feedback = 'This alias already exists'
      } else {
        ref.set({ alias: this.alias, geolocation: null })
        .then(() => {
          return firebase.auth().createUserWithEmailAndPassword(this.email, this.password)
        })
        .then(cred => {
          return ref.update({ user_id: cred.user.uid })
        })
        .then(() => {
          this.$router.push({ name: 'GMap' })
        })
        .catch(err => {
          this.feedback = err.message
        })


        // ****************old code that no longer works********************
        // firebase.auth().createUserWithEmailAndPassword(this.email, this.password)
        // .then(cred => {
        //   ref.set({
        //     alias: this.alias,
        //     geolocation: null,
        //     user_id: cred.user.uid
        //   })
        //   console.log("cred.user = ",cred.user)
        // }).then(() => {
        //   this.$router.push({ name: 'GMap' })
        // })
        // .catch(err => {
        //   console.log(err)
        //   this.feedback = err.message
        // })
        // this.feedback = 'This alias is free to use'
      }
    })
    console.log(this.slug)
  } else {
    this.feedback = "You must enter all fields"
  }
}
```

---

## 84: Wrapping the Vue Instance

- updated so that firebase has a chance to connect

### main.js

```js
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import firebase from 'firebase'

Vue.config.productionTip = false

let app = null;

// wait for firebase auth to init before creating app
firebase.auth().onAuthStateChanged(() => {

  // init app if not already created
  if (!app) {
    new Vue({
      router,
      render: h => h(App)
    }).$mount('#app')
  }
})
```

---

## 85: Logging a User Out

- set up logout in Navbar

```vue
<template>
  <div>
    <nav class="deep-purple darken-1">
      <div class="container">
        <a href="" class="brand-logo left">GeoNinjas!</a>
        <ul class="right">
          <li><router-link :to="{ name: 'Signup' }">Signup</router-link></li>
          <li><a href="">Login</a></li>
          <li><a @click="logout">Logout</a></li>
        </ul>
      </div>
    </nav>
  </div>
</template>

<script>
import firebase from 'firebase'
export default {
  name: 'Navbar',
  data() {
    return {

    }
  },
  methods: {
    logout () {
      firebase.auth().signOut().then(() => {
        this.$router.push({
          name: 'Signup'
        })
      })
    }
  }
}
</script>
```

---

## 86: Login Component

### added new Login page

```vue
<template>
  <div class="login container">
    <form @submit.prevent="login" class="card-panel">
      <h2 class="center deep-purple-text">Login</h2>
      <div class="field">
        <label for="email">Email:</label>
        <input type="email" name="email" v-model="email">
      </div>
      <div class="field">
        <label for="password">Password:</label>
        <input type="password" name="password" v-model="password">
      </div>
      <p v-if="feedback" class="red-text center">{{ feedback }}</p>
      <div class="field">
        <button class="btn deep-purple">Login</button>
      </div>
    </form>
  </div>
</template>

<script>
export default {
  name: 'Login',
  data () {
    return {
      email: null,
      password: null,
      feedback: null
    }
  },
  methods: {
    login () {
      console.log(this.email, this.password)
    }
  }
}
</script>

<style>
  .login {
    max-width: 400px;
    margin-top: 50px;
  }
  .login h2 {
    font-size: 2.4em;
  }
  .login .field {
    margin-bottom: 16px;
  }
</style>
```

### updated router/index.js

```js
import Vue from 'vue'
import VueRouter from 'vue-router'
import GMap from '@/components/home/GMap'
import Signup from '@/components/auth/Signup'
import Login from '@/components/auth/Login'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'GMap',
    component: GMap
  },
  {
    path: '/signup',
    name: 'Signup',
    component: Signup
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
```

### updated Navbar

```vue
<template>
  <div>
    <nav class="deep-purple darken-1">
      <div class="container">
        <a href="" class="brand-logo left">GeoNinjas!</a>
        <ul class="right">
          <li><router-link :to="{ name: 'Signup' }">Signup</router-link></li>
          <li><router-link :to="{ name: 'Login' }">Login</router-link></li>
          <li><a @click="logout">Logout</a></li>
        </ul>
      </div>
    </nav>
  </div>
</template>

<script>
import firebase from 'firebase'
export default {
  name: 'Navbar',
  data() {
    return {

    }
  },
  methods: {
    logout () {
      firebase.auth().signOut().then(() => {
        this.$router.push({
          name: 'Signup'
        })
      })
    }
  }
}
</script>
```

---

## 87: Logging Users In

- logging user to console

### updated login method in Login.vue

```js
methods: {
  login () {
    if (this.email && this.password) {
      firebase.auth().signInWithEmailAndPassword(this.email, this.password)
        .then(cred => {
          console.log(cred.user)
          this.$router.push({ name: 'GMap'})
        }).catch(err => {
          this.feedback = err.message
        })
      this.feedback = null
    } else {
      this.feedback = "Please fill in both fields"
    }
  }
}
```

![image](images/Lesson087.png)

---

## 88: Geolocation API

- updated map to show user's location!

### GMap.vue

```js
mounted() {
  // get user geoloc
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(pas => {
      this.lat = pas.coords.latitude
      this.lng = pas.coords.longitude
      this.renderMap()
    }, (err) => {
      console.log(err)
      this.renderMap()
    }, { maximumAge: 60000, timeout: 3000 })
  } else {
    // position centre by default values
    this.renderMap()
  }
}
```

---

## 89: Updating a User's Location

- updated map
- logs user's location in Firebase now!

### GMap.vue

```js
mounted() {
  // get current user
  let user = firebase.auth().currentUser


  // get user geoloc
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(pos => {
      this.lat = pos.coords.latitude
      this.lng = pos.coords.longitude


      // find user record & update coords
      db.collection('users').where('user_id', '==', user.uid).get()
      .then(snapshot => {
        snapshot.forEach((doc) => {
          db.collection('users').doc(doc.id).update({
            geolocation: {
              lat: pos.coords.latitude,
              lng: pos.coords.longitude
            }
          })
        })
      }).then(() => {
        this.renderMap()
      })

      this.renderMap()
    }, (err) => {
      console.log(err)
      this.renderMap()
    }, { maximumAge: 60000, timeout: 3000 })
  } else {
    // position centre by default values
    this.renderMap()
  }
}
```

---

## 90: Route Guarding (auth)

- added route guard

### routes/index.js

```js
import Vue from 'vue'
import VueRouter from 'vue-router'
import GMap from '@/components/home/GMap'
import Signup from '@/components/auth/Signup'
import Login from '@/components/auth/Login'
import firebase from 'firebase'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'GMap',
    component: GMap,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/signup',
    name: 'Signup',
    component: Signup
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  // check to see if route requires auth
  if (to.matched.some(rec => rec.meta.requiresAuth)) {
    // check auth state
    let user = firebase.auth().currentUser
    if (user) {
      // signed in, then proceed
      next()
    } else {
      // not signed in
      next({ name: 'Login' })
    }
  } else {
    next()
  }
})
export default router
```

---

## 91: Conditional Navbar Links

### Navbar.vue

```vue
<template>
  <div>
    <nav class="deep-purple darken-1">
      <div class="container">
        <router-link :to="{ name: 'GMap' }" class="brand-logo left">GeoNinjas!</router-link>
        <ul class="right">
          <li v-if="!user"><router-link :to="{ name: 'Signup' }">Signup</router-link></li>
          <li v-if="!user"><router-link :to="{ name: 'Login' }">Login</router-link></li>
          <li v-if="user"><a href="">{{ user.email }}</a></li>
          <li v-if="user"><a @click="logout">Logout</a></li>
        </ul>
      </div>
    </nav>
  </div>
</template>

<script>
import firebase from 'firebase'
export default {
  name: 'Navbar',
  data() {
    return {
      user: null
    }
  },
  methods: {
    logout () {
      firebase.auth().signOut().then(() => {
        this.$router.push({
          name: 'Login'
        })
      })
    }
  },
  created() {
    //let user = firebase.auth().currentUser
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.user = user
      } else {
        this.user = null
      }
    })
  }
}
</script>
```

---

## 92: Ninja (map) Markers

- added to methods in

### GMap.vue

```js
db.collection('users').get().then(users => {
  users.docs.forEach(doc => {
    let data = doc.data()
    if (data.geolocation) {
      let marker = new google.maps.Marker({
        position: {
          lat: data.geolocation.lat,
          lng: data.geolocation.lng
        },
        map
      })
      // add click event to marker
      marker.addListener('click', () => {
        console.log(doc.id)
      })

    }
  })
})
```

---

## 93: User Profiles

- added profile page

### ViewProfile.vue

```vue
<template>
  <div class="view-profile container">
    <div v-if="profile" class="card">
      <h2 class="deep-purple-text center">{{ profile.alias }}'s Wall</h2>
    </div>
  </div>
</template>

<script>
import db from '@/firebase/init'
export default {
  name: 'ViewProfile',
  data () {
    return {
      profile: null,
    }
  },
  created () {
    let ref = db.collection('users')
    ref.doc(this.$route.params.id).get()
    .then(user => {
      this.profile = user.data()
    })
  }
}
</script>

<style>

</style>
```

- added line to the GMap method

```js
this.$router.push({name: 'ViewProfile', params: { id: doc.id }})
```

---

## 94: User Comments (data discussion)

- added a new collection in the Firebase database

---

## 95: Adding Comments

- updated ViewProfile
- comments are now storing in Firebase, but not showing up on the wall yet

```vue
<template>
  <div class="view-profile container">
    <div v-if="profile" class="card">
      <h2 class="deep-purple-text center">{{ profile.alias }}'s Wall</h2>
      <ul class="comments collection">
        <li>Comment</li>
      </ul>
      <form @submit.prevent="addComment">
        <div class="field">
          <label for="comment">Add a comment</label>
          <input type="text" name="comment" v-model="newComment">
          <p v-if="feedback" class="red-text center">{{ feedback }}</p>
        </div>
      </form>
    </div>
  </div>
</template>

<script>
import db from '@/firebase/init'
import firebase from 'firebase'
export default {
  name: 'ViewProfile',
  data () {
    return {
      profile: null,
      newComment: null,
      feedback: null,
      user: null
    }
  },
  created () {
    let ref = db.collection('users')

    // get current user
    ref.where('user_id', '==', firebase.auth().currentUser.uid).get()
    .then(snapshot => {
      snapshot.forEach(doc => {
        this.user = doc.data(),
        this.user.id = doc.id
      })
    })

    ref.doc(this.$route.params.id).get()
    .then(user => {
      this.profile = user.data()
    })
  },
  methods: {
    addComment () {
      if (this.newComment) {
        this.feedback = null
        db.collection('comments').add({
          to: this.$route.params.id,
          from: this.user.id,
          content: this.newComment,
          time: Date.now()
        }).then(() => {
          this.newComment = null
        })
      } else {
        this.feedback = 'You must enter a comment to add it'
      }
    }
  }
}
</script>
```

---

## 96: Showing Comments (Real-Time)

- pretty clueless at this point. Nothing works. Really fucking frustrated. Even tried copying code directly from the course.

---

## 97: Some Final Styles

- Added styles, but it still doesn't work

---

## 98: Deploying the App

- no point in deploying a broken app

---
