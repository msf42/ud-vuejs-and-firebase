# Project Two - Real-Time Chat App

## 58: Project Overview & Setup

- we'll set up a real time database that uses a listener to automatically pull data
- set up new project using CLI

---

## 59: Project Structure

![image](./images/Lesson059.png)

---

## 60: Firestore Setup

- basic firestore setup
- created init.js

### init.js

```js
import firebase from 'firebase'
import firestore from 'firebase/firestore'

var firebaseConfig = {
  apiKey: 'AIzaSyCvwRvbTw15gYOME57Q0nYXGJmTP0pmOPk',
  authDomain: 'udemy-ninja-chat-aa4e5.firebaseapp.com',
  databaseURL: 'https://udemy-ninja-chat-aa4e5.firebaseio.com',
  projectId: 'udemy-ninja-chat-aa4e5',
  storageBucket: 'udemy-ninja-chat-aa4e5.appspot.com',
  messagingSenderId: '380664430233',
  appId: '1:380664430233:web:1b4cb19dfb9f2c1b65c208'
}
// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig)
firebaseApp.firestore().settings({ timestampsInSnapshots: true })

export default firebaseApp.firestore()

```

---

## 61: Making a Welcome Screen

- set up welcome page
- logs the name to console

![image](images/Lesson061.png)

### Welcome.vue (renamed from HelloWorld.vue)

```vue
<template>
  <div class="welcome container">
    <div class="card">
      <div class="card-content center-align">
        <h2 class="teal-text">Welcome</h2>
        <form @submit.prevent="enterChat">
          <label for="name">Enter your name</label>
          <input type="text" name="name" v-model="name">
          <button class="btn teal">Enter Chat</button>
        </form>
      </div>
    </div>
  </div>
</template>

<script>
export default {
  name: 'Welcome',
  data () {
    return {
      name: null
    }
  },
  methods: {
    enterChat () {
      console.log(this.name);

    }
  }
}
</script>

<!-- Add "scoped" attribute to limit CSS to this component only -->
<style>
.welcome {
  max-width: 400px;
  margin-top: 100px;
}

.welcome h2 {
  font-size: 3em;
}

.welcome button {
  margin: 30px auto;
}
</style>
```

---

## 62: Passing Props Via Routes

### Welcome.vue

```vue
<template>
  <div class="welcome container">
    <div class="card">
      <div class="card-content center-align">
        <h2 class="teal-text">Welcome</h2>
        <form @submit.prevent="enterChat">
          <label for="name">Enter your name</label>
          <input type="text" name="name" v-model="name">
          <p class="red-text" v-if="feedback">{{ feedback }}</p>
          <button class="btn teal">Enter Chat</button>
        </form>
      </div>
    </div>
  </div>
</template>

<script>
export default {
  name: 'Welcome',
  data () {
    return {
      name: null,
      feedback: null
    }
  },
  // name is entered, passed as a prop, and used to display the chat
  methods: {
    enterChat () {
      if (this.name) {
        this.$router.push({
          name: 'Chat',
          params: {
            name: this.name
          }
        })
      } else {
        this.feedback = 'You must enter a name to join'
      }
    }
  }
}
</script>
```

### Chat.vue (new file)

```vue
<template>
  <div class="chat container">
    <h2>Chat {{ this.name }}</h2>
  </div>
</template>

<script>
export default {
  name: 'Chat',
  props: ['name'],
  data () {
    return {

    }
  }
}
</script>
```

### index.js (added Chat to router)

```js
import Vue from 'vue'
import Router from 'vue-router'
import Welcome from '@/components/Welcome'
import Chat from '@/components/Chat'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Welcome',
      component: Welcome
    },
    {
      path: '/chat',
      name: 'Chat',
      component: Chat,
      props: true
    }
  ]
})
```

---

## 63: Route Guards

### index.js

- added route guard to prevent access to chat if name is not entered

```js
import Vue from 'vue'
import Router from 'vue-router'
import Welcome from '@/components/Welcome'
import Chat from '@/components/Chat'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Welcome',
      component: Welcome
    },
    {
      path: '/chat',
      name: 'Chat',
      component: Chat,
      props: true,
      beforeEnter: (to, from, next) => {
        if (to.params.name) {
          next()
        } else {
          next({ name: 'Welcome' })
        }
      }
    }
  ]
})
```

---

## 64: Creating the Chat Window

- created the box that will show the chat content
- added styling for it

### Chat.vue

```vue
<template>
  <div class="chat container">
    <h2 class="center teal-text">Ninja Chat</h2>
    <div class="card">
      <div class="card-content">
        <ul class="messages">
          <li>
            <span class="teal-text">Name</span>
            <span class="gray-text text-darken-3">message</span>
            <span class="grey-text time">time</span>
          </li>
        </ul>
      </div>
      <div class="card-action">
        <input type="text">
      </div>
    </div>
  </div>
</template>

<script>
export default {
  name: 'Chat',
  props: ['name'],
  data () {
    return {

    }
  }
}
</script>

<style>
.chat h2 {
  font-size: 2.5em;
  margin-bottom: 40px;
}
.chat span {
  font-size: 1.4em;
}
.chat .time {
  display: block;
  font-size: 1.2em;
}
</style>
```

---

## 65: New Message Component

- created NewMessage.vue
- added it to Chat.vue

### Chat.vue

```vue
<template>
  <div class="chat container">
    <h2 class="center teal-text">Ninja Chat</h2>
    <div class="card">
      <div class="card-content">
        <ul class="messages">
          <li>
            <span class="teal-text">Name</span>
            <span class="gray-text text-darken-3">message</span>
            <span class="grey-text time">time</span>
          </li>
        </ul>
      </div>
      <div class="card-action">
        <NewMessage :name="name" />
      </div>
    </div>
  </div>
</template>

<script>
import NewMessage from '@/components/NewMessage'
export default {
  name: 'Chat',
  props: ['name'],
  components: {
    NewMessage
  },
  data () {
    return {

    }
  }
}
</script>
```

### NewMessage.vue

```vue
<template>
  <div class="new-message">
    <form @submit.prevent="addMessage">
      <label for="new-message">New Message (enter to add)</label>
      <input type="text" name="new-message" v-model="newMessage">
    </form>
  </div>
</template>

<script>
export default {
  name: 'NewMessage',
  props: ['name'],
  datea () {
    return {
      newMessage: null
    }
  },
  methods: {
    addMessage () {
      console.log(this.newMessage, this.name, Date.now())

    }
  }
}
</script>
```

---

## 66: Adding Messages to Firestore

- updated NewMessage to send messages to firestore

### NewMessage.vue

```vue
<template>
  <div class="new-message">
    <form @submit.prevent="addMessage">
      <label for="new-message">New Message (enter to add)</label>
      <input type="text" name="new-message" v-model="newMessage">
      <p class="red-text" v-if="feedback">{{ feedback }}</p>
    </form>
  </div>
</template>

<script>
import db from '@/firebase/init'
export default {
  name: 'NewMessage',
  props: ['name'],
  data () {
    return {
      newMessage: null,
      feedback: null
    }
  },
  methods: {
    addMessage () {
      if (this.newMessage) {
        db.collection('messages').add({
          content: this.newMessage,
          name: this.name,
          timestamp: Date.now()
        }).catch(err => {
          console.log(err)
        })
        this.newMessage = null
        this.feedback = null
      } else {
        this.feedback = 'You must enter a message in order to send one'
      }
    }
  }
}
</script>
```

---

## 67: Real-Time Events (event listeners)

- now shows chats in real time as they are entered
- also sorts them by time

### Chat.vue

```vue
<template>
  <div class="chat container">
    <h2 class="center teal-text">Ninja Chat</h2>
    <div class="card">
      <div class="card-content">
        <ul class="messages">
          <li v-for="message in messages" :key="message.id">
            <span class="teal-text">{{ message.name}}</span>
            <span class="gray-text text-darken-3">{{ message.content}}</span>
            <span class="grey-text time">{{ message.timestamp }}</span>
          </li>
        </ul>
      </div>
      <div class="card-action">
        <NewMessage :name="name" />
      </div>
    </div>
  </div>
</template>

<script>
import NewMessage from '@/components/NewMessage'
import db from '@/firebase/init'
export default {
  name: 'Chat',
  props: ['name'],
  components: {
    NewMessage
  },
  data () {
    return {
      messages: []
    }
  },
  created () {
    let ref = db.collection('messages').orderBy('timestamp')

    ref.onSnapshot(snapshot => {
      snapshot.docChanges().forEach(change => {
        if (change.type === 'added') {
          let doc = change.doc
          this.messages.push({
            id: doc.id,
            name: doc.data().name,
            content: doc.data().content,
            timestamp: doc.data().timestamp
          })
        }
      })
    })
  }
}
</script>
```

---

## 68: Formatting Times with Moment

[momentjs](momentjs.com)

- changed the timestamp in Chat.vue to format with moment.js

```js
timestamp: moment(doc.data().timestamp).format('YYYY MMMM Do, h:mm:ss a')
```

---

## 69: Auto-scrolling

- installed vue-chat-scroll
- registered in main.js
- easy to use
  - just add `vue-chat-scroll` to any element

---

## 70: Deploying the App

- firebase init
- npm run build
- firebase deploy

---

## 71: Project Review

Done

---
