# Vue Router

## 27: What is the Vue Router

- just set up a vue router project using `vue init webpack [project name]`

---

## 28: Setting up Routes

### index.js

- created About.vue page and added it to routes

```js
import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import About from '@/components/About'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/about',
      name: 'About',
      component: About
    }
  ]
})
```

---

## 29: Router Links

### Navbar.vue

```vue
<template>
  <nav class="main-nav">
    <ul>
    <!-- bind the links here instead of hard-coding them -->
      <li><router-link :to="{ name: 'Home' }">Home</router-link></li>
      <li><router-link :to="{ name: 'About'}">About</router-link></li>
    </ul>
  </nav>
</template>

<script>
export default {
  name: 'Navbar',
  data () {
    return {

    }
  }
}
</script>

<style>
ul {
  list-style-type: none;
  padding: 0
}
a{
  color: #42b983;
}
</style>
```

---

## 30: Route Parameters

### ViewProfile

- new file

```vue
<template>
  <div id="view-profile">
    <h2>Profile for {{ userId }}</h2>
  </div>
</template>

<script>
export default {
  name: 'ViewProfile',
  data () {
    return {
      userId: this.$route.params.user_id
    }
  }
}
</script>
```

### index.js

- added path for ViewProfile

```js
import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import About from '@/components/About'
import ViewProfile from '@/components/ViewProfile'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/profile/:user_id',
      name: 'ViewProfile',
      component: ViewProfile
    }
  ]
})
```

---

## 31: Watching the `$route` Object

### ViewProfile.vue

- simply added a `watch` and a `method` to keep Id updated
- otherwise, the component doesn't get reloaded when we change the address

![video](./images/Lesson031.gif)

---

## 32: More on Router Links

### Navbar.vue

- added router links

```html
<router-link :to="{ name: 'ViewProfile', params: { user_id: id }}">
  <span>Profile {{ id }}</span>
</router-link>
```

---

## 33: Programatically Redirecting Users

### Navbar.vue

- added router navigation methods

```vue
<template>
  <nav class="main-nav">
    <ul>
      <li><router-link :to="{ name: 'Home' }">Home</router-link></li>
      <li><router-link :to="{ name: 'About'}">About</router-link></li>
    </ul>
    <h2>User Profiles</h2>
    <ul>
      <li v-for="(id, index) in userIds" :key="index">
        <router-link :to="{ name: 'ViewProfile', params: { user_id: id }}">
          <span>Profile {{ id }}</span>
        </router-link>
      </li>
    </ul>
    <h2>Navigation Controls</h2>
    <ul>
      <li><button @click="goBack">Go Back</button></li>
      <li><button @click="goHome">Redirect to Home</button></li>
      <li><button @click="goForward">Go Forward</button></li>
    </ul>
  </nav>
</template>

<script>
export default {
  name: 'Navbar',
  data () {
    return {
      userIds: ['1', '2', '3', '4']
    }
  },
  methods: {
    goHome () {
      this.$router.push({ name: 'Home' })
    },
    goBack () {
      this.$router.go(-1)
    },
    goForward () {
      this.$router.go(+1)
    }
  }
}
</script>

<style>
ul {
  list-style-type: none;
  padding: 0
}
a{
  color: #42b983;
}
li{
  display: inline-block;
  margin: 10px;
}
</style>
```

![image](./images/Lesson032.png)

---

## 34: Hash vs History Mode

- just reviewed the reason for `/#/` in the address bar
- keeps browser from making requests every time we change pages

---

## 35: Styling Active Links

### Navbar.vue

- simply added a CSS class

```css
a.router-link-exact-active{
  color: purple;
}
```

---
