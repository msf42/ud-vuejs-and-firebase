# Build Web Apps with Vue.js and Firebase

- Section 01: Intro

- [Section 02: Basics](02-Basics.md)

- [Section 03: Vue CLI](03-VueCLI.md)

- [Section 04: Vue Router](04-VueRouter.md)

- [Section 05: Project One](05-ProjectOne.md)

- [Section 06: Project Two](06-ProjectTwo.md)

- [Section 07: Project Three](07-ProjectThree.md)

- Section 08: Firebase Cloud Functions - No notes for this section

- Section 09: ES6 & Extras - REview, no notes

- [Section 10: BONUS: Vue CLI 3](10-BONUS:%20Vue%20CLI%203.md)

---
