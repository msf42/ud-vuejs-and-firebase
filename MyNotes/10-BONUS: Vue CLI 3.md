# Vue CLI 3

## 107: Vue CLI 3 Introduction

### Some differences between 2 and 3

- old cli: `vue init webpack-simple myapp`
- new cli: `vue create myapp`
  - default preset uses eslint and babel
  - custom preset can use vuex, vue router, pwa support, etc

### Webpack Config

- Webpack config abstracted away (hidden)
- Tweak webpack config in vue.config.js
- Plugins can edit the webpack config too

### Plugins

- Extend the config and functionality of our app
- Like normal dependencies, but can also
  - edit the webpack config
  - edit source files (e.g. templates)
  - add extra commands to the CLI

### Instant Prototyping

- Rapidly prototype single, stand-alone components
- No need to set up a Vue project to develop single components
- Good when you quickly want to work on an idea

### Web Components

- Build single components or whole projects into web components
- Re-use those web components in other projects
- Drop them in as custom HTML tags

### GUI

- Easily create and manage projects
- Manage and install dependencies and plugins

---

## 108: Using the new Vue CLI

- Installing - `npm install -g @vue/cli`
- The `@` represents the "Vue Scope", and we are installing the "cli" package within that scope

- installed and created new app

---

## 109: The CLI Service

- just reviewed the JSON file and other defaults

---

## 110: Custom Presets

- set up a custom preset

---

## 111: Adding Plugins

- Done

---

## 112: Build and Deploy to Firebase

- Review, already did this earlier in the course

---

## 113: Instant Prototyping

- Vue CLI Service Global
  - `npm install -g @vue/cli-service-global`
  - allows us to preview components
  - they don't even need to be part of a project
  - `vue serve mycomponent.vue`

---

## 114: Build Targets (Making a Web Component)

- App
  - Default build target to build a full Vue app
- Library
  - Build target to build a Vue component library
- Web Component
  - Target to build a standalone web component
  - These can be used in other projects, independent of the other project's library
  - `<steves-component></steves-component>`
  - `vue build stevescomponent.vue --target wc --name my-component-name`

---

## 115: Using Web Components

- reference vue library
- reference our javascript (`stevescomponent.js`)
- put our web component in with our custom component name
  - we can pass in props as usual

---

## 116 and 117: The Vue GUI

- `vue ui` opens the GUI
- pretty straightforward

---

## 118: Using `vue init` with the new CLI

- in order to use old command, we must install
  - `npm install -g @vue/cli-init`

---
