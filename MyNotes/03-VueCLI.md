# The Vue CLI

## 16: The Vue CLI

### A Better Workflow with Webpack

- Use ES6 Features that are not already supported
- Compile and minify code into 1 file
- Use single-file Vue templates
- Use a live reload development server

- at this point I jumped to last section on Vue CLI 3
  - did lessons 107 - 1

---

## 17: Components and Vue Files

- `npm run dev` for development version

## main.js

```js
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
// takes the App.vue and renders it in the #app id
```

- app.vue is *always* rendered to the browser
  - the 'root' component

---

## 18: The `data()` Function

- when we place the data object in a component, it is wrapped in a function to protect it from being changed if we use the component multiple times

### app.vue

```vue
<template>
  <div id="app">
    <h1>{{ title }}</h1>
  </div>
</template>

<script>
export default {
  name: 'App',
  data: () => ({
    title: 'My first Vue app!!'
  }),
};
</script>

<style>
h1 {
  color: #444;
  font-weight: normal;
  text-align: center;
}
</style>
```

---

## 19: Nesting Components

- made a Navbar component

### App.vue

```vue
<template>
  <div id="app">
    <h1>{{ title }}</h1>
    <Navbar />
  </div>
</template>

<script>
import Navbar from './Navbar'
export default {
  name: 'app',
  components: {
    Navbar
  },
  data: () => ({
    title: 'My first Vue app!!'
  }),
};
</script>

<style>
h1 {
  color: #444;
  font-weight: normal;
  text-align: center;
}
</style>
```

### Navbar.vue

```vue
<template>
  <nav>
    <ul>
      <li><a href="">Home</a></li>
      <li><a href="">About</a></li>
      <li><a href="">Contact</a></li>
    </ul>
  </nav>
</template>

<script>
export default {
  name: 'Navbar',
  data(){
    return {

    }
  }
}
</script>

<style scoped>
nav{
  text-align: center;
}
nav ul{
  padding: 0;
}
nav li{
  display: inline-block;
  list-style-type: none;
  margin: 0;
}
</style>
```

---

## 20: Scoped CSS

- just a quick review of scoped CSS
- author recommends against it, instead use classes, which you can specify in your CSS

---

## 21: Passing Data with Props

### App.vue

```vue
<template>
  <div id="app">
    <h1>{{ title }}</h1>
    <Navbar />
    <AllFriends :friends="friends" />
    <OnlineFriends :friends="friends" />
  </div>
</template>

<script>
import Navbar from './Navbar'
import AllFriends from './AllFriends'
import OnlineFriends from './OnlineFriends'
export default {
  name: 'app',
  components: {
    Navbar,
    AllFriends,
    OnlineFriends
  },
  data: () => ({
    title: 'My first Vue app!!',
    friends: [
        { name: 'Mario', online: true },
        { name: 'Luigi', online: false },
        { name: 'Toad', online: true },
        { name: 'Bowser', online: false },
      ]
  }),
};
</script>

<style>
h1 {
  color: #444;
  font-weight: normal;
  text-align: center;
}
</style>
```

### OnlineFriends.vue

```vue
<template>
  <div id="online-friends">
    <h2>Online Friends</h2>
    <div v-for="(friend, index) in friends" :key="index">
      <span v-if="friend.online">{{ friend.name }}</span>
    </div>
  </div>
</template>

<script>
export default {
  name: 'OnlineFriends',
  props: [
    'friends'
  ],
  data() {
    return {

    }
  }
}
</script>

<style>
  
</style>
```

### AllFriends.vue

```vue
<template>
  <div id="all-friends">
    <h2>All Friends</h2>
    <div v-for="(friend, index) in friends" :key="index">
      <span>{{ friend.name }}</span>
    </div>
  </div>
</template>

<script>
export default {
  name: 'AllFriends',
  props: [
    'friends' // recieving prop from app.vue
  ],
  data() {
    return {

    }
  }
}
</script>
```

![image](images/Lesson021.png)

---

## 22: Custom Events

### added in AllFriends.vue

```html
<span @click="unfriend(friend.name)">{{ friend.name }}</span>

<!-- added unfriend method and emitted `delete` with the `name` argument -->

methods: {
    unfriend(name) {
      this.$emit('delete', { name })
    }
  }
```

### added in App.vue

```html
<AllFriends :friends="friends" @delete="deleteFriend"/>
<!-- listened for `delete` -->

<!-- `payload` is the argument passed in; here we filter and return true if the friend's name is NOT in the list -->
methods: {
    deleteFriend(payload){
      console.log(payload)
      this.friends  = this.friends.filter(friend => {
        return friend.name !== payload.name
      })
    }
  }
```

![Video](images/Lesson022.gif)

---

## 23: Life-Cycle Hooks

[Life Cycle Diagram](https://vuejs.org/v2/guide/instance.html#Lifecycle-Diagram)

### App.vue - just imported Blogs.vue

### Blogs.vue

```vue
<template>
  <div class="blogs">
    <h2>{{ blogTitle }}</h2>
    <button @click="changeTitle">Change Title</button>
  </div>
</template>

<script>
export default {
  name: 'Blogs',
  data() {
    return {
      blogTitle: 'Blogs'
    }
  },
  methods: {
    changeTitle() {
      this.blogTitle = 'Amazing Blog Site'
    }
  },
  beforeCreate() {
    alert('beforeCreate')
  },
  created() {
    alert('created')
  },
  beforeUpdate() {
    alert('beforeUpdate')
  }
}
</script>
```

![video](./images/Lesson023.gif)

---

## 24: Making Requests with Axios

[JSON Placeholder](https://jsonplaceholder.typicode.com/)

### Blogs.vue

```vue
<template>
  <div class="blogs">
    <h2>Blogs</h2>
    <div v-for="post in posts" :key="post.id">
      <h3>{{ post.title }}</h3>
      <p>{{ post.body }}</p>
    </div>
  </div>
</template>

<script>
import axios from 'axios'
export default {
  name: 'Blogs',
  data() {
    return {
      posts: []
    }
  },
  methods: {
  },
  created() {
    axios.get('https://jsonplaceholder.typicode.com/posts/')
      .then(response => {
        this.posts = response.data
      }).catch(err => {
        console.log(err)
      })
  }
}
</script>
```

---

## 25: Filters

### added filter in Blogs.vue

```html
<p>{{ post.body | snippet}}</p>
```

### created the filter in main.js

```js
// create global filter
Vue.filter('snippet', val => {
  if (!val || typeof(val) != 'string') return ''
  val = val.slice(0,50)
  return val
})
```

---

## 26: Computed Properties (custom search box)

### Blogs.vue

```vue
<template>
  <div class="blogs">
    <h2>Blogs</h2>
    <input type="text" v-model="searchTerm"> <!-- added searchbox -->
    <div v-for="post in filteredPosts" :key="post.id"> <!-- replaced w filteredPosts -->
      <h3>{{ post.title }}</h3>
      <p>{{ post.body | snippet}}</p>
    </div>
  </div>
</template>

<script>
import axios from 'axios'
export default {
  name: 'Blogs',
  data() {
    return {
      posts: [],
      searchTerm: '' // added default blank search
    }
  },
  methods: {
  },
  /////////////////////////////////
  // added the computed property to filter posts that match search
  computed: {
    filteredPosts() {
      return this.posts.filter(post => {
        return post.title.match(this.searchTerm)
      })
    }
  },
  /////////////////////////////////
  created() {
    axios.get('https://jsonplaceholder.typicode.com/posts/')
      .then(response => {
        this.posts = response.data
      }).catch(err => {
        console.log(err)
      })
  }
}
</script>
```

![Video](images/Lesson026.gif)

---
