import firebase from 'firebase'
import firestore from 'firebase/firestore'

// Initialize Firebase
const firebaseConfig = {
  apiKey: "AIzaSyBHXuTzkj9_Jy_71swYdp1q0-_m9tFS9Po",
  authDomain: "geo-ninjas-3387b.firebaseapp.com",
  databaseURL: "https://geo-ninjas-3387b.firebaseio.com",
  projectId: "geo-ninjas-3387b",
  storageBucket: "geo-ninjas-3387b.appspot.com",
  messagingSenderId: "839561956603",
  appId: "1:839561956603:web:db30a083f910a01072b45e"
};
// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);
// firebaseApp.firestore().settings({ timestampsInSnapshots: true })
export default firebaseApp.firestore()