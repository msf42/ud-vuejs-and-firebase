import firebase from 'firebase'
// import firestore from 'firebase/firestore'

// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: 'AIzaSyACbp9gvGpghWfQw_nIO_aQBoMbIRhM7oE',
  authDomain: 'udemy-ninja-smoothies-1af92.firebaseapp.com',
  databaseURL: 'https://udemy-ninja-smoothies-1af92.firebaseio.com',
  projectId: 'udemy-ninja-smoothies-1af92',
  storageBucket: 'udemy-ninja-smoothies-1af92.appspot.com',
  messagingSenderId: '18734731671',
  appId: '1:18734731671:web:3c3a588407a03a57e874d2'
}
// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig)
// export firestore database
export default firebaseApp.firestore()
