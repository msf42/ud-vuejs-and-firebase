import firebase from 'firebase'
// import firestore from 'firebase/firestore'

var firebaseConfig = {
  apiKey: 'AIzaSyCvwRvbTw15gYOME57Q0nYXGJmTP0pmOPk',
  authDomain: 'udemy-ninja-chat-aa4e5.firebaseapp.com',
  databaseURL: 'https://udemy-ninja-chat-aa4e5.firebaseio.com',
  projectId: 'udemy-ninja-chat-aa4e5',
  storageBucket: 'udemy-ninja-chat-aa4e5.appspot.com',
  messagingSenderId: '380664430233',
  appId: '1:380664430233:web:1b4cb19dfb9f2c1b65c208'
}
// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig)
firebaseApp.firestore().settings({ timestampsInSnapshots: true })

export default firebaseApp.firestore()
